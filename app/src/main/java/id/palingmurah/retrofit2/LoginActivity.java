package id.palingmurah.retrofit2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.palingmurah.retrofit2.Api.ApiClient;
import id.palingmurah.retrofit2.Api.ApiInterface;
import id.palingmurah.retrofit2.Model.User;
import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    String TAG = LoginActivity.class.getSimpleName();
    ApiInterface apiInterface;
    EditText usernameEditText;
    EditText passwordEditText;
    Button loginButton;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sharedPreferences = getSharedPreferences("credentials",MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if(sharedPreferences != null && sharedPreferences.getString("username",null) != null){
            Intent mainIntent = new Intent(this,MainActivity.class);
            startActivity(mainIntent);
            finish();
        }
        setContentView(R.layout.activity_login);
        usernameEditText = (EditText) findViewById(R.id.username_edittext);
        passwordEditText = (EditText) findViewById(R.id.password_edittext);
        loginButton = (Button) findViewById(R.id.btn_login);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                onBtnLoginClicked();
                break;
        }
    }

    void onBtnLoginClicked(){
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if(username.equals("")){
            usernameEditText.setError("Username is required");
            return;
        }
        if(password.equals("")){
            passwordEditText.setError("Password is required");
//            return;
        }
        Call<User> responseBodyCall = apiInterface.login(username,password);
        Log.d(TAG, "onBtnLoginClicked: responseBodyCall.request().headers().toString() = "+responseBodyCall.request().headers().toString());
        responseBodyCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()){
                    setPreferences(response.body().getNama());
                }else {
                    Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.fillInStackTrace());
            }
        });
    }
    public void setPreferences(String nama){
        editor.putString("username",nama);
        editor.commit();
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
