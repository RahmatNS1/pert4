package id.palingmurah.retrofit2.Model;

import java.util.List;

/**
 * Created by rahma on 17/12/2017.
 */

public class ResponseUser {
    List<User> DataUser;

    public List<User> getDataUser() {
        return DataUser;
    }

    public void setDataUser(List<User> dataUser) {
        DataUser = dataUser;
    }
}
