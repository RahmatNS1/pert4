package id.palingmurah.retrofit2.Model;

import java.util.List;

/**
 * Created by rahma on 09/12/2017.
 */

public class ResponseKota {
    List<Kota> DataKota;

    public List<Kota> getDataKota() {
        return DataKota;
    }

    public void setDataKota(List<Kota> dataKota) {
        DataKota = dataKota;
    }
}
