package id.palingmurah.retrofit2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.palingmurah.retrofit2.Api.ApiClient;
import id.palingmurah.retrofit2.Api.ApiInterface;
import id.palingmurah.retrofit2.Model.Kota;
import id.palingmurah.retrofit2.Model.ResponseKota;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    ApiInterface apiInterface;
    List<Kota> listKota = new ArrayList<>();
    RecyclerView recyclerView;
    KotaAdapter kotaAdapter;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sharedPreferences = getSharedPreferences("credentials",MODE_PRIVATE);
        if(sharedPreferences == null || sharedPreferences.getString("username",null) == null){
            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }

        kotaAdapter = new KotaAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(kotaAdapter);
        getDataKota();
    }

    public void getDataKota(){
        Call<ResponseKota> responseBodyCall = apiInterface.getdatakota();
        responseBodyCall.enqueue(new Callback<ResponseKota>() {
            @Override
            public void onResponse(Call<ResponseKota> call, Response<ResponseKota> response) {
                if(response.isSuccessful()){
                    ResponseKota responseKota = response.body();
                    listKota = responseKota.getDataKota();
                    Log.d(TAG, "onResponse: responseKota.getDataKota().size() = "+responseKota.getDataKota().size());
                    kotaAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(MainActivity.this, "Request error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseKota> call, Throwable t) {
                Log.d(TAG, "onFailure: fillInStackTrace() = "+t.fillInStackTrace());
                Log.d(TAG, "onFailure: getLocalizedMessage() = "+t.getLocalizedMessage());
                Log.d(TAG, "onFailure: getMessage() = "+t.getMessage());
            }
        });
    }

    class KotaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        class KotaViewHolder extends RecyclerView.ViewHolder{
            TextView textView;
            KotaViewHolder(View view){
                super(view);
                textView = (TextView) view.findViewById(R.id.text_view);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.this.onClick(v,getAdapterPosition());
                    }
                });
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((KotaViewHolder)holder).textView.setText(
                    listKota.get(position).getKota()
            );
        }

        @Override
        public int getItemCount() {
            return listKota.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.kota_item_layout,parent,false);
            KotaViewHolder kotaViewHolder = new KotaViewHolder(view);
            return kotaViewHolder;
        }
    }
    public void onClick(View view, int position){
        Bundle bundle = new Bundle();
        bundle.putString("City",listKota.get(position).getKota());

        Intent userListIntent = new Intent(MainActivity.this,UserList.class);
        userListIntent.putExtras(bundle);

        startActivity(userListIntent);

    }
}
