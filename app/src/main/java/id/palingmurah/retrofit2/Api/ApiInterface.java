package id.palingmurah.retrofit2.Api;

import java.util.List;

import id.palingmurah.retrofit2.Model.ResponseKota;
import id.palingmurah.retrofit2.Model.ResponseUser;
import id.palingmurah.retrofit2.Model.User;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rahma on 09/12/2017.
 */

public interface ApiInterface {
    @GET("/belajarretrofit/getdatakota.php")
    Call<ResponseKota> getdatakota();

    @GET("belajarretrofit/getdatauserbykota.php")
    Call<ResponseUser> getUserByCity(@Query("vskota") String kota);

    @GET("belajarretrofit/getdatauserbynama.php")
    Call<ResponseUser> getUserByNama(@Query("nama") String nama);

    @POST("belajarretrofit/pert4/index.php/api/example/login")
    @FormUrlEncoded
    Call<User> login(@Field("username") String username, @Field("password") String password);

    @Multipart
    @POST("belajarretrofit/pert4/index.php/welcome/upload")
    Call<ResponseBody> setImage (
            @Part("username") String username,
            @Part MultipartBody.Part file);
}
