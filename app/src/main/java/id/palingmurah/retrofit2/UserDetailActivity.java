package id.palingmurah.retrofit2;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import id.palingmurah.retrofit2.Api.ApiClient;
import id.palingmurah.retrofit2.Api.ApiInterface;
import id.palingmurah.retrofit2.Model.ResponseUser;
import id.palingmurah.retrofit2.Model.User;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDetailActivity extends AppCompatActivity {
    String TAG = UserDetailActivity.class.getSimpleName();
    String currentNama;
    User currentUser;
    ApiInterface apiInterface;
    ImageView userImage;
    TextView userNama;
    TextView userEmail;
    TextView userKota;

    NetworkInfo activeNetwork;
    ConnectivityManager connectivityManager;

    String username;
    Uri fileUri;
    ProgressDialog uploadProgress;

//    Sensor
    private SensorManager mSensorManager;

//    Bluetooth
    private static final int REQUEST_ENABLE_BT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences("credentials",MODE_PRIVATE);
        username = sharedPreferences.getString("username",null);
        if(username == null){
            Intent intent = new Intent(UserDetailActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

//        Bluetooth

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null){
            Log.d(TAG, "onCreate: Device doesnt support bluetooth");
        }else{
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

//        Discovering devices
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(blueToothReceiver, filter);

        connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        activeNetwork = connectivityManager.getActiveNetworkInfo();

        if(activeNetwork!=null &&
                activeNetwork.isConnectedOrConnecting()){
            Toast.makeText(this, "You are connected", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "You are not connected to internet", Toast.LENGTH_LONG).show();
        }
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                activeNetwork = connectivityManager.getActiveNetworkInfo();
                if(activeNetwork!=null &&
                        activeNetwork.isConnectedOrConnecting()){
                    if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI){
                        Log.d(TAG, "onReceive: Wifi is connected");
                    }
                    Toast.makeText(context, "Welcome back", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(context, "You are not connected to internet", Toast.LENGTH_LONG).show();
                }
            }
        },new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        currentNama = getIntent().getStringExtra("nama");
        getSupportActionBar().setTitle(currentNama);

        setContentView(R.layout.activity_user_detail);
        userImage = (ImageView) findViewById(R.id.user_image);
        userNama = (TextView) findViewById(R.id.user_nama);
        userEmail = (TextView) findViewById(R.id.user_email);
        userKota = (TextView) findViewById(R.id.user_kota);

        getUserDetailsByNama();
    }
    void getUserDetailsByNama(){
        Call<ResponseUser> responseUserCall = apiInterface.getUserByNama(currentNama);
        responseUserCall.enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                ResponseUser responseUser = response.body();
                List<User> users = responseUser.getDataUser();
                currentUser = users.get(0);
                Log.d(TAG, "onResponse: currentUser.getImage() = "+currentUser.getImage());
                Glide.with(UserDetailActivity.this).load(currentUser.getImage()).into(userImage);
                userNama.setText(currentUser.getNama());
                userEmail.setText(currentUser.getEmail());
                userKota.setText(currentUser.getKota());
            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {

            }
        });
    }
    public void onCameraClicked(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,0);
    }
    public void onGalleryClicked(View view){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && data.getData() != null){
            try{
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),data.getData());
                Drawable imageDrawable = new BitmapDrawable(getResources(),bitmap);
                userImage.setImageDrawable(imageDrawable);
                fileUri = data.getData();
                sendProfileImage();
            }catch (Exception e){
                Log.d(TAG, "onActivityResult: e.fillInStackTrace() = "+e.fillInStackTrace());
            }
        }
    }
    public void sendProfileImage() {
        if(uploadProgress == null){
            uploadProgress = new ProgressDialog(this);
            uploadProgress.setCanceledOnTouchOutside(false);
            uploadProgress.setCancelable(false);
            uploadProgress.setMessage("Mengirim Gambar");
        }
        uploadProgress.show();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(fileUri, filePathColumn, null, null, null);
        if (cursor == null)
            return;
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();

        File file = new File(filePath);
        final RequestBody requestBody = RequestBody.create(
                MediaType.parse(getContentResolver().getType(fileUri)), file
        );
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        Call<ResponseBody> profileCall = apiInterface.setImage(username, body);
        profileCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse: "+response.raw());
                try {
                    JSONObject response_json = new JSONObject(response.body().string());
                    Log.d(TAG, "onResponse: response_json = "+response_json);
                    if (response_json.get("result").equals("success")) {
                        onSetSelfieSuccess();
                    } else {
                        onSetSelfieError();
                    }
                } catch (Exception e) {
                    Log.d(TAG, "onResponse: e = "+e.fillInStackTrace());
                    onSetSelfieError();
                }
                uploadProgress.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onSetSelfieFailed();
                Log.d(TAG, "onFailure: ");
                uploadProgress.dismiss();
            }
        });
    }
    public void onSetSelfieSuccess(){
        Snackbar.make(userNama,"Sukses upload image",Snackbar.LENGTH_LONG).show();
    }
    public void onSetSelfieError(){
        Snackbar.make(userNama,"Error saat upload image",Snackbar.LENGTH_LONG)
                .setAction("Coba Lagi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendProfileImage();
                    }
                })
                .show();
    }
    public void onSetSelfieFailed(){
        Snackbar.make(userNama,"Gagal mengirim image",Snackbar.LENGTH_LONG)
                .setAction("Coba Lagi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendProfileImage();
                    }
                })
                .show();
    }
    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver blueToothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Don't forget to unregister the ACTION_FOUND receiver.
        unregisterReceiver(blueToothReceiver);
    }
}
