package id.palingmurah.retrofit2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.palingmurah.retrofit2.Api.ApiClient;
import id.palingmurah.retrofit2.Api.ApiInterface;
import id.palingmurah.retrofit2.Model.Kota;
import id.palingmurah.retrofit2.Model.ResponseUser;
import id.palingmurah.retrofit2.Model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserList extends AppCompatActivity {
    String TAG = UserList.class.getSimpleName();
    String currentCity;
    ApiInterface apiInterface;
    RecyclerView userListRecycler;
    UserListAdapter userListAdapter;
    List<User> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentCity = getIntent().getStringExtra("City");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        setContentView(R.layout.activity_user_list);

        userListRecycler = (RecyclerView) findViewById(R.id.user_list_recycler);
        userListRecycler.setLayoutManager(new LinearLayoutManager(UserList.this));
        userListAdapter = new UserListAdapter();
        userListRecycler.setAdapter(userListAdapter);

        getSupportActionBar().setTitle("User List");
        getUserByCity();
    }

    class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        class UserListHolder extends RecyclerView.ViewHolder{
            TextView textView;
            UserListHolder(View view){
                super(view);
                textView = (TextView) view.findViewById(R.id.text_view);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserList.this.onClick(v,getAdapterPosition());
                    }
                });
            }
        }
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((UserListHolder)holder).textView.setText(userList.get(position).getNama());
        }

        @Override
        public int getItemCount() {
            return userList.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(UserList.this).inflate(R.layout.kota_item_layout,parent,false);
            return new UserListHolder(view);
        }
    }

    void getUserByCity(){
        Call<ResponseUser> responseBodyCall = apiInterface.getUserByCity(currentCity);
        responseBodyCall.enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                Log.d(TAG, "onResponse: response.raw() = "+response.raw());
                ResponseUser responseUser = response.body();
                userList.addAll(responseUser.getDataUser());
                Log.d(TAG, "onResponse: userList.size() = "+userList.size());
                userListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {

            }
        });
    }
    void onClick(View view, int position){
        Bundle bundle = new Bundle();
        bundle.putString("nama",userList.get(position).getNama());

        Intent intent = new Intent(UserList.this,UserDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
